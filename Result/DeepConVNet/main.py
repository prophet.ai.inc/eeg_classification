from plot_tool import plot_all_csv_in_folder
if __name__ == "__main__": 
    plot_all_csv_in_folder("train_acc", "Train Accuracy", "acc" )
    plot_all_csv_in_folder("train_loss", "Train Loss", "loss" )
    plot_all_csv_in_folder("test_acc", "Test Accuracy", "acc" )
