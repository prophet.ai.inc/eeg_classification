from plot_tool import plot_all_csv_in_folder
if __name__ == "__main__": 
    plot_all_csv_in_folder("train_acc/elu", "Train Accuracy", "acc" )
    plot_all_csv_in_folder("train_acc/leakyrelu", "Train Accuracy", "acc" )
    plot_all_csv_in_folder("train_acc/relu", "Train Accuracy", "acc" )
    plot_all_csv_in_folder("train_loss/elu", "Train Loss", "loss" )
    plot_all_csv_in_folder("train_loss/leakyrelu", "Train Loss", "loss" )
    plot_all_csv_in_folder("train_loss/relu", "Train Loss", "loss" )
    plot_all_csv_in_folder("test_acc/elu", "Test Accuracy", "acc" )
    plot_all_csv_in_folder("test_acc/leakyrelu", "Test Accuracy", "acc" )
    plot_all_csv_in_folder("test_acc/relu", "Test Accuracy", "acc" )