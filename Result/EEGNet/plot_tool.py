import os
import pandas as pd
import matplotlib.pyplot as plt
import itertools

def plot_all_csv_in_folder(folder_path, title, y_label):
    # 定義顏色、線段型態和標記
    colors = plt.cm.get_cmap('tab20', 20)  # 用 viridis 色彩映射，準備27種不同的顏色
    line_styles = ['-', '--', '-.', ':']
    markers = ['o', 's', '^', 'D', 'x', '+', '*']

    # 創建顏色、線段型態和標記的組合
    styles = list(itertools.product(line_styles, markers))
    
    plt.figure(figsize=(24, 9))  # 設定圖表的尺寸

    for i, filename in enumerate(os.listdir(folder_path)):
        if filename.endswith('.csv'):
            file_path = os.path.join(folder_path, filename)
            data = pd.read_csv(file_path, header=None)  # 讀取 CSV 資料，假設沒有標題行
            
            # 取得每個 CSV 檔案的第一行數據，並每十筆數據取樣一點
            sampled_data = data.iloc[0, ::25]  # 使用步長為 10 進行取樣

            # 用組合的顏色、線型和標記進行繪圖
            line_style, marker = styles[i % len(styles)]
            plt.plot(sampled_data.index, sampled_data.values, label=f'{filename}',
                     color=colors(i), linestyle=line_style, marker=marker)

    plt.legend()  # 添加圖例
    plt.title(title)  # 設定圖表標題
    plt.xlabel('Epochs')  # 設定 x 軸標籤
    plt.ylabel(y_label)  # 設定 y 軸標籤
    plt.show()
