import numpy as np
from sklearn.metrics import roc_auc_score, precision_score, recall_score, accuracy_score
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F
import torch.optim as optim

class EEGNet(nn.Module):
    def __init__(self,act='elu', alpha=0.5, ns=0.01, dp = 0.25):
        super(EEGNet, self).__init__()
        
        act_dic = {'elu' : nn.ELU(alpha=alpha), 'relu': nn.ReLU(), 'leakyrelu':nn.LeakyReLU(negative_slope = ns)}
        activate = act_dic[act]

        # 第一層卷積層
        self.firstconv = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(1, 51), stride=(1, 1), padding=(0, 25), bias=False),
            nn.BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        )

        # 深度可分卷積層
        self.depthwiseConv = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(2, 1), stride=(1, 1), groups=16, bias=False),
            nn.BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
            activate,
            #nn.ELU(alpha=0.5),
            #nn.ReLU(),
            #nn.LeakyReLU(),
            nn.AvgPool2d(kernel_size=(1, 4), stride=(1, 4), padding=0),
            nn.Dropout(p = dp)
        )

        # 分離卷積層
        self.separableConv = nn.Sequential(
            nn.Conv2d(32, 32, kernel_size=(1, 15), stride=(1, 1), padding=(0, 7), bias=False),
            nn.BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
            activate,
            #nn.ELU(alpha=0.5),
            #nn.ReLU(),
            #nn.LeakyReLU(),
            nn.AvgPool2d(kernel_size=(1, 8), stride=(1, 8), padding=0),
            nn.Dropout(p = dp)
        )

        # 分類器
        self.classify = nn.Sequential(
            nn.Linear(in_features=736, out_features=2, bias=True)
        )

    def forward(self, x):
        x = self.firstconv(x)
        x = self.depthwiseConv(x)
        x = self.separableConv(x)
        x = x.view(x.size(0), -1)  # 展平
        x = self.classify(x)
        return x
    
# (Optional) implement DeepConvNet model
class DeepConvNet(nn.Module):
    def __init__(self, C, T, N):
        super(DeepConvNet, self).__init__()
        # 根據圖片中的結構進行建模
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 25, (1, 5)),
            nn.BatchNorm2d(25, eps=1e-05, momentum=0.1),
            nn.ELU(alpha=0.5),
            nn.MaxPool2d((1, 2)),
            nn.Dropout(p=0.5)
        )
        
        self.layer2 = nn.Sequential(
            nn.Conv2d(25, 50, (1, 5)),
            nn.BatchNorm2d(50, eps=1e-05, momentum=0.1),
            nn.ELU(alpha=0.5),
            nn.MaxPool2d((1, 2)),
            nn.Dropout(p=0.5)
        )

        self.layer3 = nn.Sequential(
            nn.Conv2d(50, 100, (1, 5)),
            nn.BatchNorm2d(100, eps=1e-05, momentum=0.1),
            nn.ELU(alpha=0.5),
            nn.MaxPool2d((1, 2)),
            nn.Dropout(p=0.5)
        )

        self.layer4 = nn.Sequential(
            nn.Conv2d(100, 200, (1, 5)),
            nn.BatchNorm2d(200, eps=1e-05, momentum=0.1),
            nn.ELU(alpha=0.5),
            nn.MaxPool2d((1, 2)),
            nn.Dropout(p=0.5)
        )

        # 計算全連接層的輸入尺寸
        self.flatten_size = self._get_flatten_size(C, T)
        self.fc = nn.Linear(self.flatten_size, N)

    def _get_flatten_size(self, C, T):
        # 通過模擬前向傳播來確定扁平化之後的尺寸
        with torch.no_grad():
            input = torch.zeros((1, 1, C, T))
            input = self.layer1(input)
            input = self.layer2(input)
            input = self.layer3(input)
            input = self.layer4(input)
            return input.data.view(1, -1).size(1)

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = x.view(x.size(0), -1)  # Flatten
        x = self.fc(x)
        return F.softmax(x, dim=1)